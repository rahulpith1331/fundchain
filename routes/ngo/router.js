const express = require('express');
const controller = require('../../controller/NGO');

const route = express.Router();

route.post('/signup', controller.signup);
route.post('/signin', controller.signin);
route.post('/donationRequest', controller.requestForDonation);
route.post('/checkNgo', controller.checkNgoExist);
route.post('/dashboard', controller.dashboard);
route.post('/singlengo', controller.getNgoDetail);

route.patch('/editprofile', controller.editprofile);

module.exports = route;
