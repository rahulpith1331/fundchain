const express = require('express');
const controller = require('../../controller/view');
const viewRouter = express.Router();

viewRouter.get('/', controller.home);
viewRouter.get('/contact', controller.contactus);
viewRouter.get('/usertype', controller.usertype);

viewRouter.get('/ngo/login', controller.NgoLogin);
viewRouter.get('/ngo/registration', controller.Ngoregistration);
viewRouter.get('/ngo/dashboard', controller.Ngodashboard);
viewRouter.get('/ngo/profile', controller.NgoProfile);
viewRouter.get('/ngo/request', controller.Donationrequest);

viewRouter.get('/donor/login', controller.Donorlogin);
viewRouter.get('/donor/registration', controller.Donorregistration);
viewRouter.get('/donor/dashboard', controller.Donordashboard);
viewRouter.get('/donor/profile', controller.DonorProfile);
viewRouter.get('/donor/ngo', controller.NgoDetail);

module.exports = viewRouter;
