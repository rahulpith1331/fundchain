const { Router } = require('express');
const express = require('express');
const controller = require('../../controller/Donor');

const route = express.Router();

route.post('/signup', controller.signup);
route.post('/signin', controller.signin);
route.post('/checkdonor', controller.checkDonorExist);
route.post('/dashboard', controller.dashboard);
route.post('/ngodetail', controller.getngoDetail);

route.patch('/editprofile', controller.editprofile);

module.exports = route;
