    const web3 = require('web3');
const validation = {};

validation.isValidEmail = (sEmail) => {
    let regex = /[a-z0-9._%+-]+@[a-z0-9-]+[.]+[a-z]{2,5}$/;
    return regex.test(sEmail);
};

validation.isValidWalletAddress = (sWalletAddress) => {
    let wallet = web3.utils.isAddress(sWalletAddress);
    return wallet;
};

validation.isValidPancard = (sPanCard) => {
    let regex = /[A-Z]{5}[0-9]{4}[A-Z]{1}/;
    return regex.test(sPanCard);
};

validation.isValidNGOPanCard = (sPanCard) => {
    let regex = /[A-Z]{5}[0-9]{4}/
    return regex.test(sPanCard)
}

module.exports = validation;