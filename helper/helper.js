const helper = {};
helper.checkingMissingInput = (oValue, aMissing) => {
    if (!oValue.sFirstName) {
        aMissing.push('Firstname');
    }

    if (!oValue.sLastName) {
        aMissing.push('LastName');
    }

    if (!oValue.sUsername) {
        aMissing.push('Username');
    }

    if (!oValue.sWalletAddress) {
        aMissing.push('Wallet Address');
    }

    if (!oValue.sGender) {
        aMissing.push('Gender');
    }

    if (!oValue.sEmail) {
        aMissing.push('Email');
    }

    if (!oValue.nAge) {
        aMissing.push('Age');
    }

    if (!oValue.sPancardNumber) {
        aMissing.push('Pancard Number');
    }

    return aMissing.length > 0 ? true : false;
};

// {
//     "sNgoName": "ABC.pvt.ltd",
//     "sWalletAddress": "0x07526ce069b1e371d40afb811c4636d23563b8be",
//     "sEmail": "abc@gmail.com",
//     "sDiscription": "this is demo ngo",
//     "sPanCardNumber": "ABCDE1234",
//     "sAccountStatus": "unblock"
// }

helper.ngoMissingValue = (oValue, aMissing) => {
    if (!oValue.sNgoName) {
        aMissing.push('NGO name');
    }

    if (!oValue.sWalletAddress) {
        aMissing.push('Wallet Address');
    }

    if (!oValue.sEmail) {
        aMissing.push('Email');
    }

    if (!oValue.sDescription) {
        aMissing.push('Description');
    }

    if (!oValue.sPanCardNumber) {
        aMissing.push('Pancard Number');
    }

    return aMissing.length > 0 ? true : false;
};

helper.requestMissingValue = (oValue, aMissing) => {
    console.log('ovalue: ', oValue);

    if (!oValue.sWalletAddress) {
        aMissing.push('Wallet Address');
    }

    if (!oValue.sDescriptionfordonation) {
        aMissing.push('Description');
    }

    if (!oValue.nTotalFundRequire) {
        aMissing.push('Funds Requirement Amount');
    }

    return aMissing.length > 0 ? true : false;
};

helper.errorMessage = (oError) => {
    let aError = oError.message;
    aError = aError.split(':');
    aError = aError.join(',');
    aError = aError.split(', ');
    aError.shift();
    let sErrorMessage = '';
    for (let i = 1; i < aError.length; i = i + 2) {
        sErrorMessage += (sErrorMessage.length ? ', ' : ' ') + aError[i];
    }
    return sErrorMessage;
};

helper.checkingMissingInputedit = (oValue, aMissing) => {
    if (!oValue.sUserId) {
        aMissing.push('sUserId');
    }

    if (!oValue.sFirstName) {
        aMissing.push('Firstname');
    }

    if (!oValue.sLastName) {
        aMissing.push('LastName');
    }

    if (!oValue.sUsername) {
        aMissing.push('Username');
    }

    if (!oValue.sWalletAddress) {
        aMissing.push('Wallet Address');
    }

    if (!oValue.sEmail) {
        aMissing.push('Email');
    }

    if (!oValue.sPancardNumber) {
        aMissing.push('Pancard Number');
    }

    return aMissing.length > 0 ? true : false;
};

module.exports = helper;
