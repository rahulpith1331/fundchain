'use strict';

$(() => {
    $('#loginmodal').modal('hide');
});

let web3 = new Web3(window.ethereum);

let sWalletAddress = '';

async function connectwallet() {
    try {
        if (window.ethereum) {
            let sAccount = await window.ethereum.request({
                method: 'eth_requestAccounts',
            });

            if (!sAccount[0]) {
                return;
            } else {
                // localStorage.setItem('sWalletAddress', sAccount[0]);
                sWalletAddress = sAccount[0];
                checkngo();
            }
        } else {
            console.log('Please install Metamask');
        }

        console.log('wallet connected successfully.....');
    } catch (err) {
        console.log(err);
    }
}

async function checkngo() {
    try {
        console.log('Checking Donors data .....');
        $.ajax({
            type: 'POST',
            url: '/api/v1/ngo/checkNgo',
            data: { sWalletAddress },
            success: function (status, result, xhr) {
                console.log(xhr);
                login();
            },
            error: function (xhr, resulr, status) {
                console.log(status);
                console.log(xhr);

                if (status === 'Not Found') {
                    location.replace('/ngo/registration');
                }
            },
        });
    } catch (err) {
        console.log(err);
    }
}

async function login() {
    try {
        console.log('login to ' + sWalletAddress);
        $.ajax({
            type: 'POST',
            url: '/api/v1/ngo/signin',
            data: { sWalletAddress },
            success: function (result, status, xhr) {
                localStorage.setItem(
                    'sNgoWalletAddress',
                    xhr.responseJSON.data.sWalletAddress
                );
                localStorage.setItem('sNgoId', xhr.responseJSON.data._id);
                location.replace('/ngo/dashboard');
            },
            error: function (xhr, result, status) {
                console.log(xhr);
            },
        });
    } catch (err) {
        console.log(err);
    }
}
