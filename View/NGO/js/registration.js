function signup() {
    try {
        console.log('signing up Donor');
        // let
        // let sUsername = $('#username-inp').val().trim();
        // let sEmail = $('#email-inp').val().trim();
        // let nAge = $('#age-inp').val().trim();
        // let sPancardNumber = $('#pancard-inp').val().trim();
        // let sWalletAddress = $('#wallet-inp').val().trim();
        // let sGender = $(`input[name = "gender"]:checked`).val().trim();

        let sNgoName = $('#ngoname-inp').val().trim();
        let sWalletAddress = $('#wallet-inp').val().trim();
        let sEmail = $('#email-inp').val().trim();
        let sDescription = $('#description-inp').val().trim();
        let sPanCardNumber = $('#pancard-inp').val().trim();

        $.ajax({
            type: 'POST',
            url: '/api/v1/ngo/signup',
            data: {
                sNgoName,
                sDescription,
                sWalletAddress,
                sEmail,
                sPanCardNumber,
            },
            success: function (result, status, xhr) {
                console.log(xhr);
                location.replace('/ngo/login');
            },
            error: function (xhr, result, status) {
                if (xhr.responseJSON.message === 'donor already exists') {
                    location.replace('/ngo/login');
                }
            },
        });
    } catch (err) {
        console.log(err);
    }
}
