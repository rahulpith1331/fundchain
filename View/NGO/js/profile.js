$(() => {
    console.log('profile detaill ...');

    let sNgoId = localStorage.getItem('sNgoId');

    $.ajax({
        type: 'POST',
        url: '/api/v1/ngo/singlengo',
        data: { sNgoId },
        success: function (result, status, xhr) {
            console.log(xhr);
            const oNgo = xhr.responseJSON.oNGO;

            $('#edit_ngoname').val(oNgo.sNgoName);
            $('#edit_email').val(oNgo.sEmail);
            $('#edit_description').val(oNgo.sDescription);
            $('#edit_walletaddress').val(oNgo.sWalletAddress);
            $('#edit_pancard').val(oNgo.sPanCardNumber);
        },

        error: function (xhr, result, status) {
            console.log(xhr);
        },
    });
});

async function editprofile() {
    try {
        console.log('edit profile call....');
        let sNgoId = localStorage.getItem('sNgoId');
        let sNgoName = $('#edit_ngoname').val().trim();
        let sEmail = $('#edit_email').val().trim();
        let sDescription = $('#edit_description').val().trim();
        let sWalletAddress = $('#edit_walletaddress').val().trim();
        let sPanCardNumber = $('#edit_pancard').val().trim();

        $.ajax({
            type: 'PATCH',
            url: '/api/v1/ngo/editprofile',
            data: {
                sNgoId,
                sNgoName,
                sDescription,
                sEmail,
                sWalletAddress,
                sPanCardNumber,
            },
            success: function (result, status, xhr) {
                console.log(xhr);
                location.reload();
            },
            error: function (xhr, result, status) {
                console.log(xhr);
            },
        });
    } catch (err) {
        console.log(err);
    }
}
