'use strict';
$(() => {
    console.log('Data table');

    $('#ngoTable').DataTable({
        responsive: false,
        processing: true,
        serverSide: true,
        ajax: {
            type: 'POST',
            url: '/api/v1/donor/ngodetail',
        },

        aoColumns: [
            {
                mData: 'sNgoName',
            },
            {
                mData: 'sWalletAddress',
            },
            {
                mData: 'sEmail',
            },
            {
                mData: 'sDescription',
            },
        ],
    });
});
