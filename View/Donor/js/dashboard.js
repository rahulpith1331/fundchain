$(() => {
    let sUserId = localStorage.getItem('sUserId');
    console.log('Welcome to dashboard');
    $.ajax({
        type: 'POST',
        url: '/api/v1/donor/dashboard',
        data: { sUserId },
        success: function (result, status, xhr) {
            console.log(xhr);
            $('#username-hdr').html(
                `Hello ${xhr.responseJSON.data.oDonor.oName.sFirstName}`
            );
            $('#numberofngoregistered').html(xhr.responseJSON.data.nNGO);
            $('#numberofdonationrequest').html(
                xhr.responseJSON.data.nDonationRequest
            );
        },
        error: function (xhr, status, result) {
            console.log(xhr);
        },
    });
});
