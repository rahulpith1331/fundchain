$(() => {
    console.log('Edit profile called......');

    const sUserId = localStorage.getItem('sUserId');

    if (!sUserId) {
        location.replace('/donor/login');
    }

    $.ajax({
        type: 'POST',
        url: '/api/v1/donor/dashboard',
        data: { sUserId },
        success: function (result, status, xhr) {
            console.log(xhr);
            $('#username-hdr').html(
                `Hello ${xhr.responseJSON.data.oDonor.oName.sFirstName}`
            );

            let oDonor = xhr.responseJSON.data.oDonor;

            console.log(oDonor);

            $('#edit_firstname').val(oDonor.oName.sFirstName);
            $('#edit_lastname').val(oDonor.oName.sLastName);
            $('#edit_email').val(oDonor.sEmail);
            $('#edit_username').val(oDonor.sUsername);
            $('#edit_walletaddress').val(oDonor.sWalletAddress);
            $('#edit_pancard').val(oDonor.sPancardNumber);
        },
        error: function (xhr, status, result) {
            console.log(xhr);
            alert(xhr.response.responseJSON.message);
        },
    });
});
async function editprofile() {
    try {
        console.log('edit profile call....');
        let sUserId = localStorage.getItem('sUserId');
        let sFirstName = $('#edit_firstname').val().trim();
        let sLastName = $('#edit_lastname').val().trim();
        let sEmail = $('#edit_email').val().trim();
        let sUsername = $('#edit_username').val().trim();
        let sWalletAddress = $('#edit_walletaddress').val().trim();
        let sPancardNumber = $('#edit_pancard').val().trim();

        console.log(
            'sFirstname >> ' + sFirstName,
            'sLastname >>' + sLastName,
            'sEmail >>' + sEmail,
            'sUsername >>' + sUsername,
            'sWalletAddress >>' + sWalletAddress,
            'sPancard >>' + sPancardNumber
        );

        $.ajax({
            type: 'PATCH',
            url: '/api/v1/donor/editprofile',
            data: {
                sUserId,
                sFirstName,
                sLastName,
                sEmail,
                sWalletAddress,
                sPancardNumber,
                sUsername,
            },
            success: function (result, status, xhr) {
                console.log(xhr);
                location.reload();
            },
            error: function (xhr, result, status) {
                console.log(xhr);
            },
        });
    } catch (err) {
        console.log(err);
    }
}
