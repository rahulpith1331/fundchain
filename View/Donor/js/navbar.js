'use strict';

$(() => {
    let sWalletAddress = localStorage.getItem('sWalletAddress');
    if (sWalletAddress) {
        sWalletAddress =
            sWalletAddress.substring(0, 5) +
            '...' +
            sWalletAddress.substring(27, 32);
        console.log(sWalletAddress);
        $('#connect-btn').hide();
        $('#disconnect-btn').show();
        $('#disconnect-btn').html(sWalletAddress);
    }
});

function gotologinpage() {
    location.href = '/login';
}

function disconnectwallet() {
    localStorage.removeItem('sWalletAddress');
    location.reload();
    console.log('walletdisconnected');
}
