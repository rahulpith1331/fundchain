'use strict';

$(() => {
    let sNgoId = localStorage.getItem('sNgoId');

    $.ajax({
        type: 'POST',
        url: '/api/v1/ngo/dashboard',
        data: { sNgoId },
        success: function (result, status, xhr) {
            console.log(xhr);
            // $('#username-hdr').html(
            //     `Hello ${xhr.responseJSON.data.oDonor.oName.sFirstName}`
            // );
            $('#numberofdonationrequest').html(
                xhr.responseJSON.nDonationRequestCount
            );
            $('#donationrequestapproved').html(
                xhr.responseJSON.nDonationRequestAppovalCount
            );
            $('#donationrequestsreported').html(
                xhr.responseJSON.nTotalRequestReported
            );
            $('#totaldonationreceived').html(
                xhr.responseJSON.nDonationRequestReceivedFund
            );
        },
        error: function (xhr, status, result) {
            console.log(xhr);
            alert(xhr.response.responseJSON.message);
        },
    });
});
