function signup() {
    try {
        console.log('signing up Donor');
        let sFirstName = $('#firstname-inp').val().trim();
        let sLastName = $('#lastname-inp').val().trim();
        let sUsername = $('#username-inp').val().trim();
        let sEmail = $('#email-inp').val().trim();
        let nAge = $('#age-inp').val().trim();
        let sPancardNumber = $('#pancard-inp').val().trim();
        let sWalletAddress = $('#wallet-inp').val().trim();
        let sGender = $(`input[name = "gender"]:checked`).val().trim();

        $.ajax({
            type: 'POST',
            url: '/api/v1/donor/signup',
            data: {
                sFirstName,
                sLastName,
                nAge,
                sUsername,
                sWalletAddress,
                sEmail,
                sPancardNumber,
                sGender,
            },
            success: function (result, status, xhr) {
                console.log(xhr);
                location.replace('/donor/login');
            },
            error: function (xhr, result, status) {
                if (xhr.responseJSON.message === 'donor already exists') {
                    location.replace('/donor/login');
                }
            },
        });
    } catch (err) {
        console.log(err);
    }
}
