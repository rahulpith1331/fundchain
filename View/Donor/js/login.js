'use strict';

$(() => {});

let web3 = new Web3(window.ethereum);

let sWalletAddress = '';

async function connectwallet() {
    try {
        if (window.ethereum) {
            let sAccount = await window.ethereum.request({
                method: 'eth_requestAccounts',
            });

            if (!sAccount[0]) {
                return;
            } else {
                sWalletAddress = sAccount[0];
                checkDonor();
                // localStorage.setItem('sWalletAddress', sAccount[0]);
                // $('#loginmodal').modal('show');
            }
        } else {
            console.log('Please install Metamask');
        }

        console.log('wallet connected successfully.....');
    } catch (err) {
        console.log(err);
    }
}

async function checkDonor() {
    try {
        console.log('Checking user data please wait...');
        $.ajax({
            type: 'POST',
            url: '/api/v1/donor/checkDonor',
            data: { sWalletAddress },
            success: function (status, result, xhr) {
                console.log(xhr);
                login();
            },
            error: function (xhr, resulr, status) {
                console.log(status);
                console.log(xhr);

                if (status === 'Not Found') {
                    location.replace('/donor/registration');
                }
            },
        });
    } catch (err) {
        console.log(err);
    }
}

async function login() {
    try {
        console.log(sWalletAddress);
        $.ajax({
            type: 'POST',
            url: '/api/v1/donor/signin',
            data: { sWalletAddress },
            success: function (result, status, xhr) {
                console.log(xhr);
                localStorage.setItem('sWalletAddress', sWalletAddress);
                localStorage.setItem('sUserId', xhr.responseJSON.data._id);
                location.replace('/donor/dashboard');
            },
            error: function (xhr, status, result) {
                console.log(xhr);
            },
        });
    } catch (err) {
        console.log(err);
    }
}
