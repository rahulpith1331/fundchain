const express = require('express');
const mongoose = require('mongoose');
var path = require('path');
const bodyparser = require('body-parser');
// const adminRouter = require('./Router/Admin');
const donorRouter = require('./routes/donor/router');
const ngoRouter = require('./routes/ngo/router');
const viewRouter = require('./routes/view/router');
require('dotenv').config();
// const session = require('express-session');
const app = express();

mongoose.connect(process.env.MONGODB_CONNECTION_URL);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error: '));
db.once('open', function () {
    console.log('Connected successfully');
});

// app.use(
//     session({
//         secret: process.env.TOKEN_KEY,
//         resave: true,
//         saveUninitialized: true,
//     })
// );
    
app.set('view engine', 'ejs');
app.set('views', './View');
app.use(express.static('./View/css'));
app.use(express.static(path.join(__dirname, 'View')));
app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json());

// app.use('/api/v1/admin', donorRouter);
app.use('/api/v1/donor', donorRouter);
app.use('/api/v1/ngo', ngoRouter);
app.use('/', viewRouter);

app.listen(3000, () => {
    console.clear();
    console.log('Server is running at port 3000');
});
