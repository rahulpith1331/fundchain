const controller = {};

controller.home = (req, res) => {
    res.render('common/index');
};

controller.Donorregistration = (req, res) => {
    res.render('Donor/registration');
};

controller.Ngoregistration = (req, res) => {
    res.render('NGO/registration');
};

controller.contactus = (req, res) => {
    res.render('common/contact');
};

controller.Donorlogin = (req, res) => {
    res.render('Donor/login');
};

controller.Donordashboard = (req, res) => {
    res.render('Donor/dashboard');
};

controller.Ngodashboard = (req, res) => {
    res.render('NGO/dashboard');
};

controller.usertype = (req, res) => {
    res.render('common/selectlogintype');
};

controller.NgoLogin = (req, res) => {
    res.render('NGO/login');
};

controller.DonorProfile = (req, res) => {
    res.render('Donor/profile');
};

controller.NgoProfile = (req, res) => {
    res.render('NGO/profile');
};

controller.NgoDetail = (req, res) => {
    res.render('Donor/ngo');
};

controller.Donationrequest = (req, res) => {
    res.render('NGO/donationrequest');
};
module.exports = controller;
