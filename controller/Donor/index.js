const Donor = require('../../model/donor');
const Ngo = require('../../model/NGO');
const Request = require('../../model/request');
const multer = require('multer');
const cloudinary = require('cloudinary');
const helper = require('../../helper/helper');
const { Mongoose, mongo } = require('mongoose');
require('dotenv').config();
const controller = {};

cloudinary.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.API_KEY,
    api_secret: process.env.API_SECRET,
});

const storage = multer.diskStorage({
    destination: './upload',
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    },
});

const imageUpload = multer({
    storage: storage,
    limits: {
        fileSize: 8000000, // 1000000 Bytes = 1 MB
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(png|jpg)$/)) {
            // upload only png and jpg format
            return cb(new Error('Please upload a Image'));
        }
        cb(undefined, true);
    },
}).single('sImage');

controller.signup = async (req, res) => {
    try {
        const aMissing = [];

        let oExistUser = await Donor.findOne({
            $or: [
                { sWalletAddress: req.body.sWalletAddress },
                { sPancardNumber: req.body.sPancardNumber },
            ],
        });

        if (oExistUser) {
            return res.status(403).json({ message: 'donor already exists' });
        }

        if (helper.checkingMissingInput(req.body, aMissing)) {
            res.status(419).json({
                message:
                    aMissing.length > 1
                        ? aMissing.join(', ') + ' are missing'
                        : aMissing + ' is missing',
            });
        }
        console.log(req.file);

        const oDonorData = {
            'oName.sFirstName': req.body.sFirstName,
            'oName.sLastName': req.body.sLastName,
            nAge: parseInt(req.body.nAge),
            ...req.body,
        };

        console.log(oDonorData);

        let oDonor = new Donor(oDonorData);

        let oError = oDonor.validateSync();

        if (oError) {
            return res
                .status(406)
                .json({ message: helper.errorMessage(oError) });
        }

        oDonor.save();
        return res.status(200).json({ message: 'signup successfully' });
    } catch (err) {
        console.log(err);
        return res.status(500).json('internal server error');
    }
};

controller.signin = async (req, res) => {
    try {
        console.log(req.body);
        let oDonor = await Donor.findOne({
            sWalletAddress: req.body.sWalletAddress,
        });

        if (!oDonor) {
            return res.status(404).json({ message: 'donor not found' });
        }

        if (oDonor.sAccountStatus === 'block') {
            return res
                .status(403)
                .json({ message: 'sorry! your account is block' });
        }

        console.log(oDonor);

        return res.status(200).json({
            message: 'signup successfully',
            data: { _id: oDonor._id, sRole: oDonor.sRole },
        });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ message: 'internal server error' });
    }
};

controller.checkDonorExist = async (req, res) => {
    try {
        console.log(req.body);
        if (!req.body.sWalletAddress) {
            return res.json(403).json({ message: 'Wallet-Address is missing' });
        }

        let oDonor = await Donor.findOne({
            sWalletAddress: req.body.sWalletAddress,
        });

        if (!oDonor) {
            return res.status(404).json({ message: 'Donor not found' });
        }

        return res.status(200).json({ message: 'User found Successfully' });
    } catch (err) {
        return res.status(500).json({ message: 'internal server error' });
    }
};

controller.dashboard = async (req, res) => {
    try {
        if (!req.body.sUserId) {
            return res.status(403).json({ message: 'userId is missing' });
        }

        const oDonor = await Donor.findOne({
            _id: req.body.sUserId,
        });

        if (!oDonor) {
            return res.status(400).json({ message: 'Donor not found' });
        }

        const nNGO = await Ngo.countDocuments();
        const nDonationRequest = await Request.countDocuments();

        console.log(nNGO, oDonor, nDonationRequest);
        return res.status(200).json({
            message: 'Data get successfully',
            data: { nNGO, oDonor, nDonationRequest },
        });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ message: 'internal server error' });
    }
};

controller.editprofile = async (req, res) => {
    try {
        const aMissing = [];
        console.log(req.body);
        if (helper.checkingMissingInputedit(req.body, aMissing)) {
            console.log('INSIDE IF');
            return res.status(419).json({
                message:
                    aMissing.length > 1
                        ? aMissing.join(', ') + ' are missing'
                        : aMissing + ' is missing',
            });
        }

        let oDonor = await Donor.findById({
            _id: new mongo.ObjectId(req.body.sUserId),
        });

        if (!oDonor) {
            return res.status(404).json({ message: 'Donor not found' });
        }

        let oDonorData = {
            'oName.sFirstName': req.body.sFirstName,
            'oName.sLastName': req.body.sLastName,
            ...req.body,
        };

        oDonorData = new Donor(oDonorData);

        let oError = oDonorData.validateSync();

        if (oError) {
            return res
                .status(406)
                .json({ message: helper.errorMessage(oError) });
        }

        await Donor.findOneAndUpdate(
            {
                _id: req.body.sUserId,
            },
            oDonorData
        );

        // console.log(updatedDonor);

        return res.status(200).json({ message: 'profile updated success' });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ message: 'internal server error' });
    }
};

controller.getngoDetail = async (req, res) => {
    try {
        let oSortingOrder = {};

        oSortingOrder[
            req.body.columns[parseInt(req.body.order[0].column)].data
        ] = req.body.order[0].dir === 'asc' ? 1 : -1;

        let searchStr;

        if (!req.body.search.value) {
            searchStr = {};
        } else {
            let regex = new RegExp(req.body.search.value, 'i');

            searchStr = {
                sNgoName: {
                    $regex: regex,
                },
            };
        }

        let nTotalRecords = await Ngo.countDocuments();
        let nFilterRecords = await Ngo.countDocuments(searchStr);

        let aNgo = await Ngo.aggregate([
            {
                $match: { sRequestStatus: 'unblock' },
            },
            {
                $match: searchStr,
            },
            {
                $sort: oSortingOrder,
            },
            {
                $skip: Number(req.body.start),
            },
            {
                $limit: Number(req.body.length),
            },
        ]);

        return res.status(200).json({
            data: aNgo,
            draw: req.body.draw,
            recordsTotal: nTotalRecords,
            recordsFiltered: nFilterRecords,
        });
    } catch (err) {
        return res.status(500).json({ message: 'Internal server error' });
    }
};

module.exports = controller;
