const Ngo = require('../../model/NGO');
const Request = require('../../model/request');
const helper = require('../../helper/helper');
const { default: mongoose, mongo } = require('mongoose');
const { requestMissingValue } = require('../../helper/helper');
const controller = {};

controller.signup = async (req, res) => {
    try {
        const aMissing = [];

        let oExistUser = await Ngo.findOne({
            sWalletAddress: req.body.sWalletAddress,
        });

        if (oExistUser) {
            return res.status(403).json({ message: 'NGO already exists' });
        }

        console.log(helper.ngoMissingValue(req.body, aMissing));

        if (helper.ngoMissingValue(req.body, aMissing)) {
            console.log(aMissing.length);
            return res.status(419).json({
                message:
                    (aMissing.length > 1
                        ? aMissing.join(' ,') + 'are '
                        : aMissing + ' is') + ' missing',
            });
        }

        console.log('after if');

        let oNgoData = {
            ...req.body,
        };

        oNgoData = new Ngo(oNgoData);
        let oError = oNgoData.validateSync();

        if (oError) {
            return res
                .status(406)
                .json({ message: helper.errorMessage(oError) });
        }

        oNgoData.save();

        console.log(oNgoData);
        return res.status(200).json({
            message: 'signup successfully',
        });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ message: 'internal server error' });
    }
};

controller.checkNgoExist = async (req, res) => {
    try {
        console.log(req.body);
        if (!req.body.sWalletAddress) {
            return res.json(403).json({ message: 'Wallet-Address is missing' });
        }

        let oNgo = await Ngo.findOne({
            sWalletAddress: req.body.sWalletAddress,
        });

        if (!oNgo) {
            return res.status(404).json({ message: 'Ngo not found' });
        }

        return res.status(200).json({
            message: 'Ngo found Successfully',
        });
    } catch (err) {
        return res.status(500).json({ message: 'internal server error' });
    }
};

controller.signin = async (req, res) => {
    try {
        let oNgo = await Ngo.findOne({
            sWalletAddress: req.body.sWalletAddress,
        });

        if (!oNgo) {
            return res.status(404).json({ message: 'NGO not found' });
        }

        if (oNgo.sAccountStatus === 'block') {
            return res
                .status(403)
                .json({ message: 'sorry! your account is block' });
        }

        return res.status(200).json({
            message: 'signin successfully',
            data: {
                _id: oNgo._id,
                sWalletAddress: oNgo.sWalletAddress,
            },
        });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ message: 'internal server error' });
    }
};

controller.requestForDonation = async (req, res) => {
    try {
        let aMissing = [];

        if (helper.requestMissingValue(req.body, aMissing)) {
            console.log(aMissing.length);
            return res.status(419).json({
                message:
                    (aMissing.length > 1
                        ? aMissing.join(' ,') + 'are '
                        : aMissing + ' is') + ' missing',
            });
        }

        let oNGO = await Ngo.findOne({
            sWalletAddress: req.body.sWalletAddress,
        });

        if (!oNGO) {
            return res.status(400).json({ message: 'NGO not found' });
        }

        console.log(oNGO);

        let oRequestData = {
            sNGOName: oNGO.sNgoName,
            sWalletAddress: oNGO.sWalletAddress,
            sPanCardNumber: oNGO.sPanCardNumber,
            sDescriptionfordonation: req.body.sDescriptionfordonation,
            oNGOId: oNGO._id,
            nTotalFundRequire: req.body.nTotalFundRequire,
        };

        console.log(oRequestData);

        oRequestData = new Request(oRequestData);

        oRequestData.save();

        return res.status(200).json({
            message: 'donation request raise successfully',
            data: oRequestData,
        });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ message: 'internal server error' });
    }
};

controller.dashboard = async (req, res) => {
    try {
        console.log(req.body.sNgoId);

        if (!req.body.sNgoId) {
            return res.status(419).json({ message: 'Ngo  Id is Missing' });
        }

        let nDonationRequestCount = await Request.countDocuments({
            oNGOId: {
                $eq: req.body.sNgoId,
            },
        });

        let nDonationRequestAppovalCount = await Request.countDocuments({
            oNGOId: {
                $eq: req.body.sNgoId,
            },
            sRequestStatus: {
                $eq: 'approved',
            },
        });

        let nDonationRequestReceivedFund = await Request.findOne({
            oNGOId: req.body.sNgoId,
        });

        if (!nDonationRequestReceivedFund) {
            return res.status(404).json({ message: 'Data not Found' });
        }

        console.log(nDonationRequestReceivedFund.nReport);

        let nTotalRequestReported =
            nDonationRequestReceivedFund.nReport * nDonationRequestAppovalCount;

        nDonationRequestReceivedFund =
            nDonationRequestAppovalCount *
            nDonationRequestReceivedFund.nTotalFundReceived;

        console.log(
            nDonationRequestCount,
            nDonationRequestAppovalCount,
            nDonationRequestReceivedFund,
            nTotalRequestReported
        );

        return res.status(200).json({
            message: 'Data get successfully',
            nDonationRequestCount,
            nDonationRequestAppovalCount,
            nDonationRequestReceivedFund,
            nTotalRequestReported,
        });
    } catch (err) {
        console.log(err);
        return res.status(500).json({
            message: 'internal server error',
        });
    }
};

controller.editprofile = async (req, res) => {
    try {
        const aMissing = [];

        if (!req.body.sNgoId) {
            return res.status(419).json({ message: 'ngo id is missing' });
        }

        if (helper.ngoMissingValue(req.body, aMissing)) {
            console.log('INSIDE IF');
            return res.status(419).json({
                message:
                    aMissing.length > 1
                        ? aMissing.join(', ') + ' are missing'
                        : aMissing + ' is missing',
            });
        }

        let oNgo = await Ngo.findOne({
            id: req.body.sNgoId,
        });

        if (!oNgo) {
            return res.status(404).json({ message: 'NGO not found' });
        }

        let oNgoData = {
            sNgoName: req.body.sNgoName,
            sDescription: req.body.sDescription,
            sEmail: req.body.sEmail,
            sWalletAddress: req.body.sWalletAddress,
            sPanCardNumber: req.body.sPanCardNumber,
        };

        let oError = new Ngo(oNgoData).validateSync();

        if (oError) {
            return res
                .status(406)
                .json({ message: helper.errorMessage(oError) });
        }

        await Ngo.findOneAndUpdate({ id: req.body.sNgoId }, oNgoData);

        return res.status(200).json({ message: 'profile updated success' });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ message: 'internal server error' });
    }
};

controller.getNgoDetail = async (req, res) => {
    try {
        console.log(req.body);
        if (!req.body.sNgoId) {
            return res.status(419).json({ message: 'Ngo Id is missing' });
        }

        let oNGO = await Ngo.findOne({
            _id: req.body.sNgoId,
        });

        if (!oNGO) {
            return res.status(404).json({ message: 'Ngo data not found' });
        }

        return res
            .status(200)
            .json({ message: 'Data found Successfully', oNGO });
    } catch (err) {
        return res.status(500).json({ message: 'Internal server error' });
    }
};

module.exports = controller;
