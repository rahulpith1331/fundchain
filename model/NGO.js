const mongoose = require('mongoose');
const validate = require('../helper/validation');

const NGOsSchema = new mongoose.Schema({
    sNgoName: {
        type: String,
        require: [true, 'NGO name is missing'],
    },

    sWalletAddress: {
        type: String,
        require: [true, 'walletAddress is missing'],
        unique: true,
        lowercase: true,
        validate: {
            validator: function () {
                return validate.isValidWalletAddress(this.sWalletAddress);
            },
            message: (prop) => `${prop.value} is inappropriate wallet address`,
        },
    },

    sEmail: {
        type: String,
        require: [true, 'email is missing'],
        unique: true,
        lowercase: true,
        validate: {
            validator: function () {
                return validate.isValidEmail(this.sEmail);
            },
            message: (prop) => `${prop.value} is inappropriate email`,
        },
    },

    sDescription: {
        type: String,
        require: [true, 'description is missing'],
    },

    sPanCardNumber: {
        type: String,
        uppercase: true,
        require: [true, 'pancard details is missing'],
        unique: true,
        validate: {
            validator: function () {
                return validate.isValidNGOPanCard(this.sPanCardNumber);
            },
            message: (prop) => `${prop.value} is inappropriate pancard number`,
        },
    },

    sAccountStatus: {
        type: String,
        require: [true, 'Account status is missing'],
        enum: {
            values: ['block', 'unblock'],
            message: '{VALUE} is not supported',
        },
        default: 'unblock',
    },
});

const NGO = mongoose.model('ngo', NGOsSchema);

module.exports = NGO;
