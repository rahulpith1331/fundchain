const mongoose = require('mongoose');
const validate = require('../helper/validation');

const RequestSchema = mongoose.Schema({
    sNGOName: {
        type: String,
        require: [true, 'Ngo name is missing'],
    },
    sNGOWalletAddress: {
        type: String,
        require: [true, 'wallet address is missing'],
        lowercase: true,
        validate: {
            validator: function () {
                return validate.isValidWalletAddress(this.sWalletAddress);
            },
            message: (prop) => `${prop.value} is inappropriate wallet address`,
        },
    },
    nReport: {
        type: Number,
        require: [true, 'report is missing'],
        default: 0,
    },

    sDescriptionfordonation: {
        type: String,
        require: [true, 'description is missing'],
    },

    nTotalFundRequire: {
        type: Number,
        require: [true, 'require funds is missing'],
    },

    nTotalFundReceived: {
        type: Number,
        default: 0,
    },

    oNGOId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'NGO',
    },

    sRequestStatus: {
        type: String,
        require: [true, 'request status is missing'],
        enum: {
            values: ['pending', 'rejected', 'approved'],
            message: '{VALUE} is not supported',
        },
        default: 'pending',
    },
});

const Request = mongoose.model('donation-request', RequestSchema);

module.exports = Request;
