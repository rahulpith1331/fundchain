const mongoose = require('mongoose');
const validate = require('../helper/validation')

const GovernmentSchema = new mongoose.Schema({
    sMinistryname: {
        type:String,
        require: [true, 'ministry name is missing'],
    },

    sWalletAddress: {
        type:String,
        lowercase: true,
        require: [true, 'walletaddress is missing'],
        unique: true
    },

    sEmail: {
        type:String,
        require: [true, 'emailid is missing'],
        lowercase: true,
        unique: true,
        validate: {
            validator: function () {
                return validate.isValidEmail(this.sEmail);
            },
            message: (prop) => `${prop.value} is inappropriate email`
        }
    },
})

const Government = mongoose.model('government', GovernmentSchema);

module.exports = Government;

