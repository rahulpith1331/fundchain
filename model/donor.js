const mongoose = require('mongoose');
const validate = require('../helper/validation');

const sFullName = new mongoose.Schema({
    _id: false,
    sFirstName: String,
    sLastName: String,
});

const DonorSchema = mongoose.Schema({
    oName: {
        type: sFullName,
        require: [true, 'first or last name is missing'],
    },

    sUsername: {
        type: String,
        require: [true, 'username is missing'],
    },

    sWalletAddress: {
        type: String,
        require: [true, 'walletAddress is missing'],
        lowercase: true,
        unique: true,
        validate: {
            validator: function () {
                return validate.isValidWalletAddress(this.sWalletAddress);
            },
            message: (prop) => `${prop.value} is inappropriate wallet address`,
        },
    },

    sGender: {
        type: String,
        require: [true, 'gender is missing'],
        enum: {
            values: ['male', 'female', 'others'],
            message: '{VALUE} is not supported',
        },
        require: [true, 'gender is missing'],
    },

    sEmail: {
        type: String,
        require: [true, 'Email is missing'],
        unique: true,
        lowercase: true,
        validate: {
            validator: function () {
                return validate.isValidEmail(this.sEmail);
            },
            message: (prop) => `${prop.value} is inappropriate email`,
        },
    },

    nAge: {
        type: Number,
        require: [true, 'age is missing'],
        min: 18,
    },

    sPancardNumber: {
        type: String,
        require: [true, 'pancard is missing'],
        uppercase: true,
        unique: true,
        validate: {
            validator: function () {
                return validate.isValidPancard(this.sPancardNumber);
            },

            message: (prop) => `${prop.value} is inappropriate pancard number`,
        },
    },

    sProfilepic: {
        type: String,
    },

    sAccountStatus: {
        type: String,
        require: [true, 'Account status is missing'],
        lowercase: true,
        default: 'unblock',
        enum: {
            values: ['block', 'unblock'],
            message: '{VALUE} is not supported',
        },
    },
});

const Donor = mongoose.model('donor', DonorSchema);

module.exports = Donor;
